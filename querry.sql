SELECT SV.MaSV, SV.HoSV, SV.TenSV, SV.GioiTinh, SV.NgaySinh, SV.NoiSinh, SV.DiaChi, SV.MaKH, SV.HocBong
FROM SINHVIEN SV
JOIN DMKHOA K
ON SV.MaKH = K.MaKH
WHERE K.TenKhoa = 'Cong nghe thong tin'