CREATE DATABASE QLSV;

CREATE TABLE DMKHOA (
    MaKH varchar(6) NOT NULL,
    TenKhoa varchar(30) NOT NULL,
	PRIMARY KEY (MaKH)
);

CREATE TABLE SINHVIEN (
    MaSV varchar(6) NOT NULL,
    HoSV varchar(30) NOT NULL,
    TenSV varchar(15) NOT NULL,
    GioiTinh char(1),
	NgaySinh datetime,
	NoiSinh varchar(50),
	DiaChi varchar(50),
	MaKH varchar(6),
	HocBong int,
	PRIMARY KEY (MaSV),
	FOREIGN KEY (MaKH) REFERENCES DMKHOA(MaKH)
);